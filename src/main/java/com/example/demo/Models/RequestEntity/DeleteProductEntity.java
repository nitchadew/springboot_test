package com.example.demo.Models.RequestEntity;

public class DeleteProductEntity {
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
