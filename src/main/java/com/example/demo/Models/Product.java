package com.example.demo.Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer productId;
    private String productName;
    private String productDescription;
    private String productSku;
    private int productStock;
    private int productPrice;

    public Integer getId() {
        return productId;
    }

    public void setId(Integer id) {
        this.productId = id;
    }

    public String getName() {
        return productName;
    }

    public void setName(String name) {
        this.productName = name;
    }

    public String getDescription() {
        return productDescription;
    }

    public void setDescription(String description) {
        this.productDescription = description;
    }

    public boolean allVariableValid(){
        if(this.productPrice == 0 || this.productStock == 0 || this.productSku == null || this.productDescription == null || this.productName == null) {
            return false;
        }else {
            return true;
        }
    }

    public String getSku() {
        return productSku;
    }

    public void setSku(String sku) {
        this.productSku = sku;
    }

    public int getStock() {
        return productStock;
    }

    public void setStock(int stock) {
        this.productStock = stock;
    }

    public int getPrice() {
        return productPrice;
    }

    public void setPrice(int price) {
        this.productPrice = price;
    }
}
