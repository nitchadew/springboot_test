package com.example.demo.Controllers;

import com.example.demo.Models.MyRespondEntity;
import com.example.demo.Models.Person;
import com.example.demo.Repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;


@RestController
@RequestMapping("/api/v1/test")
public class HelloSpringBootController {
    @Autowired
    private PersonRepository personRepository;

    @CrossOrigin(origins = "*")
    @GetMapping("/ping")
    public @ResponseBody ResponseEntity<Object> ping(){
        String webUrl = "www.google.com";
        MyRespondEntity mre = new MyRespondEntity();
        try{
            Process process = Runtime.getRuntime().exec("ping " + webUrl);
            printResults(process);
        }catch (Exception e){
            System.out.println("-----------exception-----------");
            System.out.println(e);
        }
        mre.setMessage("ping to " + webUrl);
        mre.setStatus("success");
        return new ResponseEntity<>(mre, HttpStatus.OK);
    }

    @GetMapping("/command")
    public @ResponseBody ResponseEntity<Object> command(){
        boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
        MyRespondEntity mre = new MyRespondEntity();
        try{
            if(isWindows){
                Process process = Runtime.getRuntime().exec("cmd /c dir", null, new File("C:\\Users\\death\\Videos"));
                printResults(process);
            }else {
                System.out.println(System.getProperty("os.name").toLowerCase());
                String[] cmd = {
                        "/bin/sh",
                        "-c",
                        "pwd && ls -a /home/ubuntu && mkdir /home/ubuntu/lnw"
                };
                Process process = Runtime.getRuntime().exec(cmd);
                printResults(process);
            }

        }catch (Exception e){
            System.out.println("-----------exception-----------");
            System.out.println(e);
        }
        mre.setMessage("");
        mre.setStatus("success");
        return new ResponseEntity<>(mre, HttpStatus.OK);
    }

    @GetMapping("/showAllPerson")
    public @ResponseBody ResponseEntity<Object> getAllPerson(){
        // This returns a JSON or XML with the users
        System.out.println("ggwp");
        MyRespondEntity mre = new MyRespondEntity();
        mre.setStatus("success");
        mre.setData(personRepository.findById(1));
        return new ResponseEntity<>(mre, HttpStatus.OK);
    }

    @PostMapping("/addPerson")
    public ResponseEntity<Object> addPerson(@RequestBody Person person){
        MyRespondEntity mre = new MyRespondEntity();
//        if(person.getName() == null){
//            mre.setStatus("error");
//            mre.setMessage("missing some parameter");
//            return new ResponseEntity<>(mre,  HttpStatus.BAD_REQUEST);
//        }
        personRepository.save(person);
        return null;
    }

    public static void printResults(Process process) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
    }

}