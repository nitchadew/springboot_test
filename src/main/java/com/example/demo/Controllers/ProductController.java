package com.example.demo.Controllers;

import com.example.demo.Models.MyRespondEntity;
import com.example.demo.Models.Product;
import com.example.demo.Models.RequestEntity.BuyProductEntity;
import com.example.demo.Models.RequestEntity.DeleteProductEntity;
import com.example.demo.Models.RequestEntity.EditStockProductEntity;
import com.example.demo.Repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;


@CrossOrigin(origins = "*")
@RestController
@ControllerAdvice
@RequestMapping("/api/v1")
public class ProductController extends ResponseEntityExceptionHandler {
    @Autowired
    private ProductRepository productRepository;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers,
    HttpStatus status, WebRequest request) {
        MyRespondEntity mre = new MyRespondEntity();
        mre.setStatus("error");
        mre.setMessage("syntax of datatype not correct.");
        System.out.println(ex);
        return new ResponseEntity<>(mre,  HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/getProduct/all")
    public @ResponseBody MyRespondEntity getAllProduct(){
        MyRespondEntity mre = new MyRespondEntity();
        mre.setStatus("success");
        mre.setData(productRepository.findAll());
        return mre;
    }

    @GetMapping("/getProduct/all/{word}")
    public @ResponseBody MyRespondEntity getAllProductByFilterName(@PathVariable String word){
        MyRespondEntity mre = new MyRespondEntity();
        mre.setStatus("success");
        mre.setData(productRepository.findByProductNameContains(word));
        return mre;
    }

    @GetMapping("/getProduct/remain")
    public @ResponseBody MyRespondEntity getRemainProduct(){
        MyRespondEntity mre = new MyRespondEntity();
        mre.setStatus("success");
        mre.setData(productRepository.findByProductStockGreaterThan(0));
        return mre;
    }

    @PostMapping("/addProduct")
    public @ResponseBody ResponseEntity<Object> addNewProduct(@RequestBody Product product){   //This means Spring is expecting a body of type Product class.
        MyRespondEntity mre = new MyRespondEntity();
        if(!product.allVariableValid()){
            mre.setStatus("error");
            mre.setMessage("some field may missing.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }

        productRepository.save(product);
        mre.setStatus("success");
        mre.setMessage("product has been added.");
        return new ResponseEntity<>(mre, HttpStatus.OK);
    }

    @PostMapping("/deleteProduct")
    public @ResponseBody ResponseEntity<Object> deleteProduct(@RequestBody DeleteProductEntity deleteProduct){
        MyRespondEntity mre = new MyRespondEntity();
        if(deleteProduct.getId() == null){
            mre.setStatus("error");
            mre.setMessage("unable to find product id.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }

        try{
            productRepository.deleteById(deleteProduct.getId());
            mre.setStatus("success");
            mre.setMessage("product has been deleted.");
            return new ResponseEntity<>(mre, HttpStatus.OK);
        }catch (EmptyResultDataAccessException e){
            System.out.println(e);
            mre.setStatus("error");
            mre.setMessage("unable to find product id.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/editProduct/stock")
    public @ResponseBody ResponseEntity<Object> editStockProduct(@RequestBody EditStockProductEntity editStock){
        MyRespondEntity mre = new MyRespondEntity();
        if(editStock.getId() == null){
            mre.setStatus("error");
            mre.setMessage("unable to find product id.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }
        try {
            Product p = productRepository.getById(editStock.getId());
            p.setStock(editStock.getStock());
            productRepository.save(p);
            mre.setStatus("success");
            mre.setMessage(p.getName() + "'s stock has been set to " + p.getStock() + " ea.");
            return new ResponseEntity<>(mre, HttpStatus.OK);
        }catch (EntityNotFoundException e){
            System.out.println(e);
            mre.setStatus("error");
            mre.setMessage("unable to find product id.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/buyProduct")
    public @ResponseBody ResponseEntity<Object> buyProduct(@RequestBody BuyProductEntity buyProduct){
        MyRespondEntity mre = new MyRespondEntity();
        if(buyProduct.getId() == null){
            mre.setStatus("error");
            mre.setMessage("unable to find product id.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }
        if(buyProduct.getNumber() <= 0){
            mre.setStatus("error");
            mre.setMessage("invalid buy number.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }

        try{
            Product p = productRepository.getById(buyProduct.getId());
            if(p.getStock() - buyProduct.getNumber() < 0){
                mre.setStatus("error");
                mre.setMessage("not enough product.");
                return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
            }else{
                p.setStock(p.getStock() - buyProduct.getNumber());
                productRepository.save(p);
                mre.setStatus("success");
                mre.setMessage("buying success.");
                return new ResponseEntity<>(mre, HttpStatus.OK);
            }
        }catch (EntityNotFoundException e){
            System.out.println(e);
            mre.setStatus("error");
            mre.setMessage("unable to find product id.");
            return new ResponseEntity<>(mre, HttpStatus.BAD_REQUEST);
        }
    }

}
