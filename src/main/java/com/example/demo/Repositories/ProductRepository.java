package com.example.demo.Repositories;

import com.example.demo.Models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query("FROM Product WHERE stock > :num")
    public Iterable<Product> myFindProductStockGreaterThen(@Param("num") int num);

    public List<Product> findByProductStockGreaterThan(int stock);

    public List<Product> findByOrderByProductPriceDesc();

    public List<Product> findByProductNameContains(String word);

}
